package de.cherriz.training.test.kino;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

import static de.cherriz.training.test.kino.Zeitfenster.*;
import static org.junit.jupiter.api.Assertions.*;

public class KinoVerwaltungTest {

    private KinoVerwaltung verwaltung;

    @BeforeEach
    public void setUp() {
        verwaltung = new KinoVerwaltung();
    }

    @DisplayName("Eine Vorstellung einplanen")
    @Test
    public void einplanenVorstellung() {
        Vorstellung vorstellung = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                  ABEND,
                                                  LocalDate.of(2018, 5, 12),
                                                  "Iron Man 4",
                                                  9.50f);
        verwaltung.einplanenVorstellung(vorstellung);

        assertEquals(1,
                     verwaltung.getVorstellungen().size(),
                     "Es wurde die falsche Anzahl registrierter Vorstellungen angegeben.");
        assertEquals(vorstellung,
                     verwaltung.getVorstellungen().get(0),
                     "Die Vorstellung wurde nicht korrekt gespeichert.");
    }

    @DisplayName("Mehrere unterschiedliche Vorstellungen einplanen.")
    @Test
    public void einplanenVorstellungMultiple() {
        Vorstellung vorstellung1 = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                   NACHMITTAG,
                                                   LocalDate.of(2018, 5, 12),
                                                   "Iron Man 4",
                                                   9.50f);

        Vorstellung vorstellung2 = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                   ABEND,
                                                   LocalDate.of(2018, 5, 12),
                                                   "Iron Man 4",
                                                   9.50f);

        Vorstellung vorstellung3 = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                   NACHT,
                                                   LocalDate.of(2018, 5, 12),
                                                   "Iron Man 4",
                                                   9.50f);

        verwaltung.einplanenVorstellung(vorstellung1);
        verwaltung.einplanenVorstellung(vorstellung2);
        verwaltung.einplanenVorstellung(vorstellung3);

        assertAll("Vorstellungen",
                  () -> assertEquals(3,
                                     verwaltung.getVorstellungen().size(),
                                     "Es wurde die falsche Anzahl registrierter Vorstellungen angegeben."),
                  () -> assertEquals(vorstellung1,
                                     verwaltung.getVorstellungen().get(0),
                                     "Die erste Vorstellung wurde nicht korrekt gespeichert."),
                  () -> assertEquals(vorstellung2,
                                     verwaltung.getVorstellungen().get(1),
                                     "Die zweite Vorstellung wurde nicht korrekt gespeichert."),
                  () -> assertEquals(vorstellung3,
                                     verwaltung.getVorstellungen().get(2),
                                     "Die dritte Vorstellung wurde nicht korrekt gespeichert."));
    }

    @DisplayName("Fehler wenn Vorestellung doppelt eingeplant.")
    @Test
    public void einplanenVorstellungDoppelt() {
        Vorstellung vorstellung = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                  ABEND,
                                                  LocalDate.of(2018, 5, 12),
                                                  "Iron Man 4",
                                                  9.50f);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            verwaltung.einplanenVorstellung(vorstellung);
            verwaltung.einplanenVorstellung(vorstellung);
        });

        assertEquals(exception.getMessage(), "Die Vorstellung ist bereits eingeplant");
        assertEquals(1,
                     verwaltung.getVorstellungen().size(),
                     "Es wurde die falsche Anzahl registrierter Vorstellungen angegeben.");
    }

    @DisplayName("KaufeTicket")
    @ParameterizedTest(name = "Reihe: {0}, Platz: {1}, Geld: {2}")
    @CsvSource({"A, 1, 10", "B, 5, 9.5", "C, 10, 9.5", "E, 1, 20", "F, 10, 100", "A, 1, 25.25"})
    public void kaufeTicket(char reihe, int platz, float geld) {
        Vorstellung vorstellung = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                  ABEND,
                                                  LocalDate.of(2018, 5, 12),
                                                  "Iron Man 4",
                                                  9.50f);
        verwaltung.einplanenVorstellung(vorstellung);

        Ticket ticket = verwaltung.kaufeTicket(vorstellung, reihe, platz, geld);

        assertAll("Ticket",
                  () -> assertEquals("Scala", ticket.getSaal()),
                  () -> assertEquals(ABEND, ticket.getZeitfenster()),
                  () -> assertEquals(LocalDate.of(2018, 5, 12), ticket.getDatum()),
                  () -> assertEquals(reihe, ticket.getReihe()),
                  () -> assertEquals(platz, ticket.getPlatz()));
    }

    @DisplayName("Massentest Ticketkauf")
    @TestFactory
    public Stream<DynamicTest> kaufeTicketDDoS() {
        KinoVerwaltung verwaltung = new KinoVerwaltung();
        verwaltung.einplanenVorstellung(new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                        NACHMITTAG,
                                                        LocalDate.of(2018, 5, 12),
                                                        "Hanni und Nanni",
                                                        9.50f));
        verwaltung.einplanenVorstellung(new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                                                        ABEND,
                                                        LocalDate.of(2018, 5, 12),
                                                        "Iron Man 4",
                                                        9.50f));


        return new Random(999)
                .ints(0, 1000)
                .limit(100)
                .mapToObj(i -> {
                    Vorstellung vorstellung = verwaltung.getVorstellungen().get(i % 2);
                    char reihe = (char) ((i % 10) + 65);
                    int platz = i % 15;
                    int geld = i % 50;

                    return DynamicTest.dynamicTest(vorstellung.getFilm() + ", " + reihe + platz + ", " + geld + "€",
                                                   () -> assertDoesNotThrow(() -> {
                                                       try {
                                                           verwaltung.kaufeTicket(vorstellung,
                                                                                  reihe,
                                                                                  platz,
                                                                                  geld);
                                                       } catch (IllegalArgumentException e) {
                                                           boolean errGeld = "Nicht ausreichend Geld.".equals(e.getMessage());
                                                           boolean errPlatz = e.getMessage()
                                                                               .contains("existiert nicht");
                                                           assertTrue(errGeld || errPlatz);
                                                       } catch (IllegalStateException e) {
                                                           assertTrue(e.getMessage()
                                                                       .contains("ist bereits belegt."));
                                                       }
                                                   }));
                });
    }

    private KinoSaal buildKinoSaal(String name, char maxReihe, int sitzeInReihe) {
        Map<Character, Integer> plaetze = new HashMap<>();
        for (char a = 'A'; a <= maxReihe; a++) {
            plaetze.put(a, sitzeInReihe);
        }
        return new KinoSaal(name, plaetze);
    }

}