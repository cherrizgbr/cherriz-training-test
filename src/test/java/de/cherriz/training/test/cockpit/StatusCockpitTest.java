package de.cherriz.training.test.cockpit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class StatusCockpitTest {

    private StatusCockpit statusCockpit;

    @BeforeEach
    public void setUp() {
        statusCockpit = new StatusCockpit();
    }

    @Test
    public void getStatusIllegalURL() {
        statusCockpit.addChecker(new AvailabilityChecker("SystemA", "--illegal--"));

        String status = statusCockpit.getStatus("SystemA");

        assertEquals("SystemA is not available (check the url --illegal--).", status, "Es wurde der falsche Status geliefert.");
    }

    @Test
    public void getStatusAvailable() {
        AvailabilityChecker checker = spy(new AvailabilityChecker("SystemB", "www.google.de"));

        doReturn(Status.AVAILABLE).when(checker).isReachable();

        statusCockpit.addChecker(checker);

        String status = statusCockpit.getStatus("SystemB");

        assertEquals("SystemB is available.", status, "Es wurde der falsche Status geliefert.");
    }

    @Test
    public void getStatusNotAvailable() {
        AvailabilityChecker checker = spy(new AvailabilityChecker("SystemC", "www.google.de"));

        doReturn(Status.NOT_AVAILABLE).when(checker).isReachable();

        statusCockpit.addChecker(checker);

        String status = statusCockpit.getStatus("SystemC");

        assertEquals("SystemC is not available.", status, "Es wurde der falsche Status geliefert.");
    }

    @Test
    public void getAllStatus() {
        AvailabilityChecker checker1 = spy(new AvailabilityChecker("SystemD", "www.google.de"));
        AvailabilityChecker checker2 = spy(new AvailabilityChecker("SystemE", "www.wikipedia.de"));

        doReturn(Status.AVAILABLE).when(checker1).isReachable();
        doReturn(Status.NOT_AVAILABLE).when(checker2).isReachable();

        statusCockpit.addChecker(checker1);
        statusCockpit.addChecker(checker2);

        List<String> allStatus = statusCockpit.getAllStatus();

        assertNotNull(allStatus, "Es wurden keine Stauts geliefert.");
        assertEquals(2, allStatus.size(), "Es wurden nicht alle oder zu viele Status geliefert.");
        assertEquals("SystemD is available.", allStatus.get(0), "Es wurde der falsche Status geliefert.");
        assertEquals("SystemE is not available.", allStatus.get(1), "Es wurde der falsche Status geliefert.");
    }

}