package de.cherriz.training.test.math;


import de.cherriz.training.test.bibliothek.Buch;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CalculatorTest {

    @Test
    public void add() {
        Calculator calc = new Calculator();

        int result = calc.add(2, 2);

        assertEquals(4, result, "Addition ermittelt falsches Ergebnis.");
    }

    @Test
    public void addNeg() {
        Calculator calc = new Calculator();

        int result = calc.add(2, -2);

        assertEquals(0, result, "Addition ermittelt falsches Ergebnis.");
    }

    @Test
    public void sub() {
        Calculator calc = new Calculator();

        int result = calc.sub(2, 2);

        assertEquals(0, result, "Subtraktion ermittelt falsches Ergebnis.");
    }

    @Test
    public void subNeg() {
        Calculator calc = new Calculator();

        int result = calc.sub(2, -2);

        assertEquals(4, result, "Subtraktion ermittelt falsches Ergebnis.");
    }

    @Test
    public void mult() {
        Calculator calc = new Calculator();

        int result = calc.mult(2, 2);

        assertEquals(4, result, "Multiplikation ermittelt falsches Ergebnis.");
    }

    @Test
    public void multNeg() {
        Calculator calc = new Calculator();

        int result = calc.mult(2, -2);

        assertEquals(-4, result, "Multiplikation ermittelt falsches Ergebnis.");
    }

    @Test
    public void div() {
        Calculator calc = new Calculator();

        int result = calc.div(2, 2);

        assertEquals(1, result, "Division ermittelt falsches Ergebnis.");
    }

    @Test
    public void divNeg() {
        Calculator calc = new Calculator();

        int result = calc.div(2, -2);

        assertEquals(-1, result, "Division ermittelt falsches Ergebnis.");
    }

    @Test
    public void divZero() {
        assertThrows(ArithmeticException.class, () -> {
            Calculator calc = new Calculator();

            calc.div(2, 0);
        });
    }

    @TestFactory
    public Stream<DynamicTest> calcTestFactory() {
        return new Random(999)
                .ints(0, 1000)
                .limit(100)
                .mapToObj(i -> DynamicTest.dynamicTest("x + x = x * 2, x = " + i, () -> {
                    Calculator calc = new Calculator();
                    assertEquals(calc.add(i, i), calc.mult(i, 2));
                }));
    }

}