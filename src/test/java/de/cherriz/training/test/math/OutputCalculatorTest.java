package de.cherriz.training.test.math;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class OutputCalculatorTest {

    private Output output;

    private OutputCalculator calc;

    @BeforeEach
    public void setUp() {
        output = mock(Output.class);
        calc = new OutputCalculator(output);
    }

    @Test
    public void add() {
        calc.add(2, 2);

        verify(output).output(eq("4"));
    }

    @Test
    public void sub() {
        calc.sub(2, 2);

        verify(output).output(eq("0"));
    }

    @Test
    public void mult() {
        calc.mult(2, 2);

        verify(output).output(eq("4"));
    }


    @Test
    public void div() {
        calc.div(2, 2);

        verify(output).output(eq("1"));
    }

}