package de.cherriz.training.test.log;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;

public class SystemLogWriterTest {

    private ErrorMonitor monitor;

    private SystemAdapter adapter;

    private SystemLogWriter logWriter;

    @BeforeEach
    public void setUp() {
        monitor = spy(new ErrorMonitorImpl());
        adapter = spy(new SystemAdapterImpl());
        logWriter = new SystemLogWriter(adapter, monitor);
    }

    @Test
    public void writeTimeLog() {
        logWriter.writeTimeLog(100L, "a");

        verify(monitor, never()).error(any(LocalDateTime.class), anyLong(), anyString());
        verify(adapter).log(any(LocalDateTime.class), eq("a"));
    }

    @Test
    public void writeTimeLogLongCall() {
        logWriter.writeTimeLog(600L, "a");

        verify(monitor).error(any(LocalDateTime.class), eq(600L), eq("a"));
        verify(adapter).log(any(LocalDateTime.class), eq("a"));
    }

}
