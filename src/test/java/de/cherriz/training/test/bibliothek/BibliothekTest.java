package de.cherriz.training.test.bibliothek;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BibliothekTest {

    private Bibliothek bibliothek;

    @BeforeEach
    public void setUp() {
        bibliothek = new Bibliothek();
        bibliothek.registrieren(new Buch("11111", "Hans", "Das Buch"));
        bibliothek.registrieren(new Buch("22222", "Uwe", "Herr der Ketten"));
        bibliothek.registrieren(new Buch("33333", "Susanne", "Das Ende"));
        bibliothek.registrieren(new Buch("33333", "Susanne", "Das Ende"));
    }

    @Test
    @DisplayName("Neues Buch Registrieren")
    public void registrieren() {
        String titel = "Java für Anfänger";
        Buch buch = new Buch("99999", "Gabi", titel);

        bibliothek.registrieren(buch);

        List<Buch> buecher = bibliothek.suchen("Java");

        assertEquals(1, buecher.size());
        assertEquals(buch, buecher.get(0));
    }

    @DisplayName("Neue Bücher Registrieren")
    @ParameterizedTest(name = "IBAN: {0}, Author: {1}, Titel: {2}")
    @CsvSource({"123456, Hans, BPMN 2.0", "456789, Uwe, PHP 5.3", "789133, Heidi, Mathematik für Informatiker"})
    void registrieren(String iban, String author, String titel) {
        Buch buch = new Buch(iban, author, titel);

        bibliothek.registrieren(buch);

        List<Buch> buecher = bibliothek.suchen(titel.split(" ")[0]);

        assertEquals(1, buecher.size());
        assertEquals(buch, buecher.get(0));
    }

    @Test
    @DisplayName("Ein Buch suchen")
    public void suchen() {
        List<Buch> buecher = bibliothek.suchen("Buch");

        assertEquals(1, buecher.size());
        assertEquals("11111", buecher.get(0).getIban());
    }

    @Test
    @DisplayName("Ein beretis geliehenes Buch suchen")
    public void suchenBereitsGeliehen() {
        bibliothek.leihen("11111", new Kunde(1L, "Host Hartman"));
        List<Buch> buecher = bibliothek.suchen("Buch");

        assertEquals(0, buecher.size());
    }

    @Test
    @DisplayName("Ein Buch leihen")
    public void leihen() {
        Buch buch = bibliothek.leihen("11111", new Kunde(1L, "Horst Hartman"));

        List<Buch> buecher = bibliothek.suchen("Buch");
        assertNotNull(buch);
        assertEquals("Horst Hartman", buch.getLeihe().getKunde().getName());
        assertTrue(buch.getLeihe().isAktiv());
        assertEquals(LocalDate.now(), buch.getLeihe().getLeihdatum());
        assertEquals("11111", buch.getIban());

        assertEquals(0, buecher.size());
    }

    @Test
    @DisplayName("Ein bereits geliehenes Buch leihen")
    public void leihenBereitsGeliehen() {
        bibliothek.leihen("11111", new Kunde(1L, "Host Hartman"));
        Buch buch = bibliothek.leihen("11111", new Kunde(1L, "Host Hartman"));

        assertNull(buch);
    }

}