package de.cherriz.training.test.notiz;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class NotizRepositoryTest {

    @Autowired
    private NotizRepository repo;

    @Test
    public void findOne() {
        Notiz notiz = repo.findById(1L).get();

        assertAll("Attribute",
                () -> assertEquals(1L, notiz.getId().longValue()),
                () -> assertEquals(1L, notiz.getUserId().longValue()),
                () -> assertEquals("Testtitel1", notiz.getTitle()),
                () -> assertEquals("Testinhalt1", notiz.getContent()));
    }

    @Test
    public void findByUser() {
        List<Notiz> notizen = repo.findByUserId(2L);

        assertEquals(2, notizen.size());
        notizen.forEach(n -> assertEquals(2L, n.getUserId().longValue()));
    }

    @Test
    public void findByContentContaining() {
        List<Notiz> notizen = repo.findByContentContaining("Schlagwort");

        assertEquals(2, notizen.size());
        notizen.forEach(n -> assertTrue(n.getContent().contains("Schlagwort")));
    }

    @Test
    public void save() {
        Notiz notiz = new Notiz(1L, "Hans", "Notizinhalt");
        Notiz notizGesp = repo.save(notiz);

        assertEquals(8L, notizGesp.getId().longValue());
        assertEquals(notiz.getUserId(), notizGesp.getUserId());
        assertEquals(notiz.getTitle(), notizGesp.getTitle());
        assertEquals(notiz.getContent(), notizGesp.getContent());
    }

    @Test
    public void update() {
        Notiz notiz = repo.findById(4L).get();

        notiz.setTitle("abc");
        notiz.setContent("def");

        repo.save(notiz);

        Notiz notiz2 = repo.findById(4L).get();

        assertEquals(notiz.getTitle(), notiz2.getTitle());
        assertEquals(notiz.getContent(), notiz2.getContent());
    }

    @Test
    public void delete() {
        repo.deleteById(5L);

        assertFalse(repo.findById(5L).isPresent());
    }

}