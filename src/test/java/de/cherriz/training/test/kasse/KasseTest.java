package de.cherriz.training.test.kasse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class KasseTest {

    private Kasse kasse;

    @BeforeEach
    public void setUp() {
        kasse = new Kasse();
        kasse.einzahlen(new Betrag(50, 0, false), new Betrag(24, 99, false));
        kasse.einzahlen(new Betrag(50, 0, false), new Betrag(24, 99, false));
        kasse.einzahlen(new Betrag(10, 0, false), new Betrag(9, 99, false));
    }

    @Test
    public void kassensturz() {
        assertEquals(5997, kasse.kassensturz(), "Kasseninhalt wird falsch berechnet.");
    }

    @Test
    public void allesEntnehmen() {
        Betrag inhalt = kasse.allesEntnehmen();
        assertEquals(59, inhalt.getEuro(), "Eurowert falsch berechnet.");
        assertEquals(97, inhalt.getCent(), "Centwert falsch berechnet.");
        assertFalse(inhalt.isNegativ(), "Abhebung darf nicht negativ sein.");
    }


}
