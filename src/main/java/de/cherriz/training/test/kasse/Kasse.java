package de.cherriz.training.test.kasse;

import java.util.LinkedList;
import java.util.List;

public class Kasse {

    private List<Betrag> transaktionsHistorie = new LinkedList<>();

    public Betrag einzahlen(Betrag geld, Betrag preis) {
        if (geld.getAsCent() < preis.getAsCent()) {
            throw new IllegalArgumentException("Zu wenig Geld.");
        }
        if (geld.getAsCent() < 0) {
            throw new IllegalArgumentException("Es darf kein negativer Betrag eingezahlt werden.");
        }
        transaktionsHistorie.add(geld);
        int rueckgeld = geld.getAsCent() - preis.getAsCent();
        transaktionsHistorie.add(new Betrag(rueckgeld * -1));
        return new Betrag(rueckgeld);
    }

    public Betrag allesEntnehmen() {
        int inhalt = kassensturz();
        transaktionsHistorie.add(new Betrag(inhalt * -1));
        return new Betrag(inhalt);
    }

    public int kassensturz() {
        return transaktionsHistorie.stream().mapToInt(Betrag::getAsCent).sum();
    }

    public Betrag wareZuruecknehmen(Betrag preis) {
        if (preis.isNegativ()) {
            throw new IllegalArgumentException("Der Produktpreis muss negativ sein.");
        }
        if (kassensturz() < preis.getAsCent()) {
            throw new IllegalStateException("Nicht genügend Geld on der Kasse.");
        }
        Betrag abhebung = new Betrag(preis.getAsCent() * -1);
        transaktionsHistorie.add(abhebung);
        return abhebung;
    }

}
