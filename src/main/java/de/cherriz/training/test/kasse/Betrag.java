package de.cherriz.training.test.kasse;

public class Betrag {

    private int euro;

    private int cent;

    private boolean negativ;

    public Betrag(int euro, int cent, boolean negativ) {
        if (cent >= 100) {
            throw new IllegalArgumentException("Nur maximal 99 Cent erlaubt.");
        }
        if (euro < 0) {
            throw new IllegalArgumentException("Negativbeträge nur über Flag setzen.");
        }
        if (cent < 0) {
            throw new IllegalArgumentException("Negativbeträge nur über Flag setzen.");
        }
        this.euro = euro;
        this.cent = cent;
        this.negativ = negativ;
    }

    Betrag(int cent) {
        this.negativ = false;
        if (cent < 0) {
            this.negativ = true;
            cent *= -1;
        }
        this.euro = cent / 100;
        this.cent = cent % 100;
    }

    public int getEuro() {
        return euro;
    }

    public int getCent() {
        return cent;
    }

    public boolean isNegativ() {
        return negativ;
    }

    public int getAsCent() {
        int b1Neg = negativ ? -1 : 1;
        int centWert = (euro * 100 + cent) * b1Neg;
        return centWert;
    }

}