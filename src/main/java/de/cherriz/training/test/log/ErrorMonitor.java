package de.cherriz.training.test.log;

import java.time.LocalDateTime;

public interface ErrorMonitor {

    void error(LocalDateTime timestamp, long duration, String task);

}
