package de.cherriz.training.test.log;

import java.time.LocalDateTime;

public class ErrorMonitorImpl implements ErrorMonitor {

    @Override
    public void error(LocalDateTime timestamp, long duration, String task) {
        System.out.println(timestamp + " " + task + ": " + duration);
    }

}