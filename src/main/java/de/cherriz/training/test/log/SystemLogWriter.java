package de.cherriz.training.test.log;

import java.time.LocalDateTime;

public class SystemLogWriter {

    private SystemAdapter system;

    private ErrorMonitor monitor;

    public SystemLogWriter (SystemAdapter systemAdapter, ErrorMonitor errorMonitor){
        this.system = systemAdapter;
        this.monitor = errorMonitor;
    }

    public void writeTimeLog(long duration, String task){
        LocalDateTime timestamp = LocalDateTime.now();
        system.log(timestamp, task);
        if(duration>500L){
            monitor.error(timestamp, duration, task);
        }
    }

}