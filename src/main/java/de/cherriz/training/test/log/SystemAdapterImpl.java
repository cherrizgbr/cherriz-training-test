package de.cherriz.training.test.log;

import java.time.LocalDateTime;

public class SystemAdapterImpl implements SystemAdapter{

    @Override
    public void log(LocalDateTime timestamp, String task) {
        System.out.println(timestamp + " " + task);
    }

}