package de.cherriz.training.test.log;

import java.time.LocalDateTime;

public interface SystemAdapter {

    void log(LocalDateTime timestamp, String task);

}
