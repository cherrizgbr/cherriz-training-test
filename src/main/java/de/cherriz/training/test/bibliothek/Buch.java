package de.cherriz.training.test.bibliothek;

public class Buch {

    private String iban;

    private String author;

    private String titel;

    private Leihe leihe;

    public Buch(String iban, String author, String titel) {
        this.iban = iban;
        this.author = author;
        this.titel = titel;
        this.leihe = null;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public Leihe getLeihe() {
        return leihe;
    }

    public void setLeihe(Leihe leihe) {
        this.leihe = leihe;
    }
}