package de.cherriz.training.test.bibliothek;

import java.time.LocalDate;

public class Leihe {

    private LocalDate leihdatum;

    private boolean aktiv;

    private Kunde kunde;

    public Leihe(Kunde kunde) {
        this.leihdatum = LocalDate.now();
        this.aktiv = true;
        this.kunde = kunde;
    }

    public LocalDate getLeihdatum() {
        return leihdatum;
    }

    public void setLeihdatum(LocalDate leihdatum) {
        this.leihdatum = leihdatum;
    }

    public boolean isAktiv() {
        return aktiv;
    }

    public void setAktiv(boolean aktiv) {
        this.aktiv = aktiv;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }

}
