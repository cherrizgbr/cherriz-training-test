package de.cherriz.training.test.cockpit;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class AvailabilityChecker {

    private String name;

    private String host;

    public AvailabilityChecker(String name, String host){
        this.name = name;
        this.host = host;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public Status isReachable() {
        try {
            if (InetAddress.getByName(host).isReachable(100)) {
                return Status.AVAILABLE;
            }
            return Status.NOT_AVAILABLE;
        } catch (UnknownHostException e) {
            return Status.ILLEGAL_URL;
        } catch (IOException e) {
            return Status.NETWORK_ERROR;
        }
    }

}
