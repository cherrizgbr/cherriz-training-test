package de.cherriz.training.test.cockpit;

public enum Status {

    ILLEGAL_URL,

    NETWORK_ERROR,

    NOT_AVAILABLE,

    AVAILABLE;

}
