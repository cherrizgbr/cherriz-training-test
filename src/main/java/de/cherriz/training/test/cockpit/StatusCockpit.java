package de.cherriz.training.test.cockpit;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StatusCockpit {

    private List<AvailabilityChecker> checkers;

    public void addChecker(AvailabilityChecker checker) {
        if (checkers == null) {
            checkers = new LinkedList<>();
        }
        checkers.add(checker);
    }

    public String getStatus(String checker) {
        Optional<String> first = checkers.stream().filter(c -> c.getName().equals(checker)).map(this::getStatusMessage).findFirst();
        return first.orElse(null);
    }

    public List<String> getAllStatus() {
        return checkers.stream().map(this::getStatusMessage).collect(Collectors.toList());
    }

    private String getStatusMessage(AvailabilityChecker checker) {
        switch (checker.isReachable()) {
            case AVAILABLE:
                return checker.getName() + " is available.";
            case NOT_AVAILABLE:
                return checker.getName() + " is not available.";
            case NETWORK_ERROR:
                return checker.getName() + " is not available (check your network).";
            case ILLEGAL_URL:
                return checker.getName() + " is not available (check the url " + checker.getHost() + ").";
        }
        return checker.getName() + " has an unknown Status.";
    }

}
