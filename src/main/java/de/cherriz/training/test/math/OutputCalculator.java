package de.cherriz.training.test.math;

public class OutputCalculator {

    private Output output;

    public OutputCalculator(Output output) {
        this.output = output;
    }

    public void add(int a, int b) {
        output.output(Integer.toString(a + b));
    }

    public void sub(int a, int b) {
        output.output(Integer.toString(a - b));
    }

    public void mult(int a, int b) {
        output.output(Integer.toString(a * b));
    }

    public void div(int a, int b) {
        output.output(Integer.toString(a / b));
    }

}
