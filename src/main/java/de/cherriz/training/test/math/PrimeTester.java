package de.cherriz.training.test.math;

public class PrimeTester {


    public boolean isPrime(int number) {
        for (int i = 2; i < number / 2; i++) {
            if (number % i == 0) {
                System.out.println(i + " teilt " + number);
                return false;
            }
        }
        return true;
    }

}
