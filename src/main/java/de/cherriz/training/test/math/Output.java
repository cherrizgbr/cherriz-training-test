package de.cherriz.training.test.math;

public interface Output {

    void output(String text);

}
