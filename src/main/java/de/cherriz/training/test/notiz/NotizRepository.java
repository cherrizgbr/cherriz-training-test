package de.cherriz.training.test.notiz;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotizRepository extends CrudRepository<Notiz, Long> {

    List<Notiz> findByUserId(Long userId);

    List<Notiz> findByContentContaining(String content);

}